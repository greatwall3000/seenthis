<?php

	class Comment 
	{
		public $id;
		public $user_id;
		public $picture_id;
		public $comment_text;
		public $created;
		
		public function __construct($c_id, $c_user_id, $c_picture_id, $c_comment_text, $c_created)
		{
			$this->id = $c_id;
			$this->user_id = $c_user_id;
			$this->picture_id = $c_picture_id;
			$this->comment_text = $c_comment_text;
			$date = new DateTime($c_created);
			$this->created = $date->format('d-m-Y');
		}

	}

?>