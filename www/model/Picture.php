<?php

	class Picture 
	{
		public $id;
		public $picture_name;
		public $link;
		public $user_id;

		
		public function __construct($pic_id, $pic_name, $pic_link, $pic_user_id)
		{
			$this->picture_name = $pic_name;
			$this->link = $pic_link;
			$this->id = $pic_id;
			$this->user_id = $pic_user_id;
		}

		public function getId(){
			return $this->id;
		}

		public function setId($id){
			$this->id = $id;
		}

		public function getPicture_name(){
			return $this->picture_name;
		}

		public function setPicture_name($picture_name){
			$this->picture_name = $picture_name;
		}

		public function getLink(){
			return $this->link;
		}

		public function setLink($link){
			$this->link = $link;
		}

		public function getUser_id(){
			return $this->user_id;
		}

		public function setUser_id($user_id){
			$this->user_id = $user_id;
		}
	
	}

?>