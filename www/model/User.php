<?php 
	
	class User
	{
		public $id;
		public $username;
		public $email;
		public $profile_picture;

		public function __construct($id, $username, $email, $profile_picture)
		{
			$this->id = $id;
			$this->username = $username;
			$this->email = $email;
			$this->profile_picture = $profile_picture;
		} 

		public function getId(){
			return $this->id;
		}

		public function setId($id){
			$this->id = $id;
		}

		public function getUsername(){
			return $this->username;
		}

		public function setUsername($username){
			$this->username = $username;
		}

		public function getEmail(){
			return $this->email;
		}

		public function setEmail($email){
			$this->email = $email;
		}

		public function getProfile_picture(){
			return $this->profile_picture;
		}

		public function setProfile_picture($profile_picture){
			$this->profile_picture = $profile_picture;
		}
	}

?>