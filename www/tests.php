<?php	
    // Enable me if somthing goes wrong
    /*
    error_reporting(E_ALL);
	ini_set('display_errors', 1);
	*/

	initTestPackages();

	function initTestPackages(){

		include_once("config.php");
		$connection = getDBC();
		if($connection){
			include("dao/UsersDao.php");

			$userDao = new UsersDao;
			$results = [];


			$results["Test_Connection"] 			= testConnection($connection);
			$results["Test_User_Table"] 			= testTableByName($connection,"users");
			$results["Test_Pictures_Table"] 		= testTableByName($connection, "pictures");
			$results["Test_Comments_Table"] 		= testTableByName($connection, "comments");
			$results["Test_Tags_Table"] 			= testTableByName($connection, "tags");
			$results["Test_Favourites_Table"] 	= testTableByName($connection, "favourites");
			$results["Test_Pictures_Tags_Table"] 	= testTableByName($connection, "pictures_tags");
			$results["Test_Relationships"]		= testRelationships($connection);

			$results["Test_Register"] 		  = TestRegister($userDao);
			$results["Test_Register_Failure"] = TestRegisterFailure($userDao);
			$results["Test_Read_Users"]		  = TestR_Users($connection);
			$results["Test_Login"]			  = TestLogin($userDao);
			$results["Test_Login_Failure"]	  = TestLoginFailure($userDao);
			$results["Test_Update_Users"] 	  = TestU_Users($connection);
			$results["Test_Delete_Users"] 	  = TestD_Users($connection);

			/*
			$results["Test_Test_Remove_Tag_Unexist"]  = TestRemoveTagById(16);
			$results["Test_Test_Remove_Tag_Used"]  = TestRemoveTagById(10);
			$results["Test_Test_Remove_Tag_Unused"]  = TestRemoveTagById(8);

			$results["Test_GetTagIdByTagName_true"] = TestGetTagIdByTagName("cat");
			$results["Test_GetTagIdByTagName_false"]= TestGetTagIdByTagName("nonexisting");
			*/
			
			$connection->close();

			printResults($results);
		}else{
			echo "<center><h2 style='background-color: red;'>
					No Connection to Database</h2></center>";
		}
	}

	// CONNECTION
	function testConnection($conn){
		if($conn){ return "Success! Connection Established";	}
		return "Connection Not Established";
	}

	// TABLES
	function testTableByName($conn, $tbl_name){

		$result 	= "Table Not Found";
		$sql 		= "SELECT `id` FROM `$tbl_name`";
		$statement 	= $conn->prepare($sql);
		
		if($statement){
			$status = $statement->execute();
			if($status){
				$statement->bind_result($rs);
				$statement->store_result();
				if( ($rows = $statement->num_rows) > 0){
					$result = "Success! Table `$tbl_name` Found (rows: $rows)";
				}else{
					$result = "Success! Table `$tbl_name` Found, but it's Empty";
				}
			}else{
				$result = "Error: Statement not Executed, Table Doesn't Exist";
			}

			$statement->fetch();
			$statement->close();
		}else{
			$result = "Error: Statement not Prepared, Check SQL statement";
		}
		
		return $result;
	}

	// JOIN TABLES
	function testRelationships($conn){

		$result = "Relationship Not Established";
		$sql = "SELECT users.username, pictures.picture_name 
				FROM users
				INNER JOIN pictures
				ON users.id=pictures.user_id";
		$statement 	= $conn->prepare($sql);
		
		if($statement){
			$rs = $statement->execute();
			if(isset($rs)){
				$statement->bind_result($username,$picture_name);
				$statement->store_result();
				if( $rows = $statement->num_rows > 0){
					$result = " Success! Relationship Rows Returned: $rows";	
				}
				//while ($myrow = $rs->fetch_assoc()){}
			}
			$statement->fetch();
			$statement->close();
		}else{
			$result = "Error: Statement not Prepared, Check SQL statement";
		}
		return $result;
	}

	// CREATE	
	function TestC_Users($conn){

		$result = "NOT DONE";
		$sql = "INSERT INTO `users` (`username`,`email`,`password`) VALUES ('someuser','anymail@one.two','password')";
		$statement 	= $conn->prepare($sql);
		
		if($statement){
			$status = $statement->execute();
			if($status){
				$result = "Success! User Created";
			}else{
				$result = "Error: Statement not Executed, User Not Created";
			}
			
			$statement->close();
			unset($status);
		}else{
			$result = "Error: Statement not Prepared, Check SQL statement";
		}
		return $result;
	}

	// READ
	function TestR_Users($conn){

		$result = "NOT DONE";
		$sql = "SELECT `id` FROM `users` WHERE `username`='someuser' ";
		$statement 	= $conn->prepare($sql);
		
		if($statement){
			$status = $statement->execute();
			
			if($status){
				$statement->bind_result($rs);
				$statement->store_result();
				if( ($rows = $statement->num_rows) > 0){
					$result = "Success! User Readable (rows: $rows)";
				}else{
					$result = "Error: Username not found, User Not Readable";
				}
			}else{
				$result = "Error: Statement not Executed, User Not Readable";
			}
			$statement->fetch();
			$statement->close();
			unset($status);
		}else{
			$result = "Error: Statement not Prepared, Check SQL statement";
		}
		return $result;
	}

	// UPDATE
	function TestU_Users($conn){

		$result = "NOT DONE";
		$sql = "UPDATE `users` SET `email`='none' WHERE `username`='someuser' ";
		$statement 	= $conn->prepare($sql);
		
		if($statement){
			$status = $statement->execute();
			if($status){
				if( $statement->affected_rows > 0){
					$result = "Success! User Updated";
				}else{
					$result = "Error: Username not found, User Not Updated";
				}
			}else{
				$result = "Error: Statement not Executed, User Not Updated";
			}
			
			$statement->close();
			unset($status);
		}else{
			$result = "Error: Statement not Prepared, Check SQL statement";
		}
		return $result;
	}

	// DELETE
	function TestD_Users($conn){

		$result = "NOT DONE";
		$sql = "DELETE FROM `users` WHERE `username`='someuser'";
		$statement 	= $conn->prepare($sql);
		
		if($statement){
			$status = $statement->execute();
			if($status){
				if( $statement->affected_rows > 0){
					$result = "Success! User Deleted";
				}else{
					$result = "Error: Username not found, User Not Deleted";
				}
			}else{
				$result = "Error: Statement not Executed, User Not Deleted";
			}
			
			$statement->close();
			unset($status);
		}else{
			$result = "Error: Statement not Prepared, Check SQL statement";
		}
		return $result;
	}

	// LOGIN
	function TestLogin($dao){
		$reply_message = $dao->login('someuser',sha1('password') );
		if(strpos($reply_message, 'Welcome') !== false){
			$reply_message = "Success! ".$reply_message;
		}else{
			$reply_message = "Error: ".$reply_message;
		}
		return $reply_message;
	}

	function TestLoginFailure($dao){
		$reply_message = $dao->login('someuser1',sha1('password') );
		if(strpos($reply_message, 'Welcome') === false){
			$reply_message = "Success! ".$reply_message;
		}else{
			$reply_message = "Error: ".$reply_message;
		}
		return $reply_message;
	}

	// REGISTER
	function TestRegister($dao){
		$reply_message = $dao->register('someuser','e@e.e','password','password');
		if(strpos($reply_message, 'Thank You') !== false){
			$reply_message = "Success! ".$reply_message;
		}else{
			$reply_message = "Error: ".$reply_message;
		}
		return $reply_message;		
	}

	function TestRegisterFailure($dao){
		$reply_message = $dao->register('someuser','e@e.e','password','password');
		if(strpos($reply_message, 'Thank You') === false){
			$reply_message = "Success! ".$reply_message;
		}else{
			$reply_message = "Error: ".$reply_message;
		}
		return $reply_message;	
	}

	function TestGetTagIdByTagName($tag_name){
		include_once("./dao/TagsDao.php");
		$tagsDao = new TagsDao();

		$result = $tagsDao->getTagIdByName($tag_name);
		
		return $result;
	}

	function TestRemoveTagById($tag_id){
		include_once("./dao/TagsDao.php");
		$tagsDao = new TagsDao();

		$result = $tagsDao->removeTagById($tag_id);
		if($result)
			return "Success!";
		else
			return "ERROR";

	}

	// PRINT RESULTS
	function printResults($array){

		echo "<center><h2>Test Results</h2><table style='font-size:1.5em;'>";
		
		$passed=0; $failed=0;

		foreach ($array as $key => $value) {
			
			if(strpos($value, 'Success!') !== false){
				echo "<tr><td style='background-color: green;'> OK </td>";
				$passed++;
			}else{
				echo "<tr><td style='background-color: red;'> XX </td>";
				$failed++;
			}
			echo "<td style='border-bottom: 1px solid;' >$key</td>";
			echo "<td style='border-bottom: 1px solid;' >$value</td></tr>";
		}

		// Display tests results as numbers percents and visual bar
		$percent =  round($passed/($passed+$failed)*100, 2);
		echo "<td colspan='3' style='text-align: center; font-weight: bold;'> $passed Tests Passed out of ". ($passed+$failed) ." (". $percent."%) </td>";
		echo "</table></center>";
		echo "<div style='width:50%; margin:auto;'>";
		echo "<div style='display:inline-block; height:10px; width:".$percent."%; background-color: green;'></div>";
		echo "<div style='display:inline-block; height:10px; width:".(100-$percent)."%; background-color: red;'></div></div>";
	}
?>