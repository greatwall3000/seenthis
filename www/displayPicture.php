<!DOCTYPE html>
<html>
<head>	
	<title> Image View </title>
<?php 
	include_once("navigation.php"); 

	if(isset($_SESSION["user_info"]) && is_array($_SESSION["user_info"])){
		$s_user_id = $_SESSION["user_info"]["user_id"];
	}
	if(isset($_GET["id"]) )
	{ 
		$g_picture_id = $_GET["id"];
	}else{
		header("Location: error.php");
	}

	include_once("./dao/PicturesDao.php");
	$current_picture = (new PicturesDao)->getPictureById($g_picture_id);

	if($current_picture == null){
		header("Location: error.php");
	}

	include_once("./dao/CommentsDao.php");
	$picture_comments = (new CommentsDao)->getCommentsByPictureId($g_picture_id);

	include_once("./dao/UsersDao.php");
	$po_id = (new PicturesDao)->getUserIdByPictureId($g_picture_id);
	$po_name = (new UsersDao)->getUsernameById($po_id);

?>
	<script>
	function deleteButtonValidation(id){
		if(confirm( "Are you sure you want to delete this picture?" )){
			window.location = "removePicture.php?pid="+id;
		}
	}
	</script>
</head>
<body>
	<div id="main" class="container">
	<div class="row text-center">
		<h2><?php echo "\"$current_picture->picture_name\"";
				  echo " by <a href='displayUser.php?u=$po_id'>$po_name</a>"; ?></h2>

	</div>
	<div class="row text-center">
		
		<div class="col-md-8">
			<div class="picture-display">
				<img class="img-rounded" src="./img/uploaded/<?php echo $current_picture->link?>"/>
				<?php
				if(isset($s_user_id) && $s_user_id == $po_id){
					echo '<a id="removeButton" style="display:block" onclick="deleteButtonValidation('.$g_picture_id.')">';
					echo '<img style="height: 32px; width: 32px" src="img/delete.png"/> Delete Picture </a>';
				}
				?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="container-fluid text-left comment-section">
				<h3> Comments:</h3>
				<?php
					include_once("./dao/UsersDao.php");
					$userDao = new UsersDao();
					foreach ($picture_comments as $comment) {
						echo "<div class='comment-header'>".$comment->created;
						echo " <a class='comment-username' href='./displayUser.php?u=$comment->user_id'>";
						echo $userDao->getUsernameById($comment->user_id);
						echo "</a> said</em>:<br/></div>";
						echo "<div class='comment-text'>".$comment->comment_text . "<br/></div><br/>";
					}
				?>
				<?php
					if(isset($s_user_id)){
						?>

				<form action="insert_comment.php" method="post">
				<input type="text" name="comment_text" class="form-control" placeholder="Write a comment..."/><br/>
				<input type="hidden" name="cmt_picture_id" value="<?php echo $g_picture_id ?>" />
				<input type="submit" />
				</form>
				<?php }else{
						?>
						<div>
							Please <a href="./login.php">Login</a> to post comments
						</div>
				<?php } ?>

			</div>
		</div>
	</div>
	<p id="spacer"></p>
	</div>
</body></html>