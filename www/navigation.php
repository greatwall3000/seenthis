<?php
session_name('LoginForm');
@session_start();
?>
<meta charset="utf-8">
<meta name='viewport' content='width=device-width, initial-scale=1'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link rel='stylesheet' type='text/css' href='css/seenthis.css' media='all' />
<script src="js/bootstrap.min.js"></script>
<script src="js/search.js"></script>
<nav class="navbar navbar-default nav-collapse" role="navigation">
    <div class="navbar-header">
        <a href="index.php">
        	<img class="navbar-logo" src="./img/logo.png"/>
        </a>
        <a class="navbar-brand" href="index.php"> SeenThis</a>
    </div>

    <div class="container-fluid">
    	<nav class="">

        <ul class="nav navbar-nav">
        	<?php 
        		if(isset($_SESSION['user_info']) && is_array($_SESSION['user_info'])) { ?>
					<li><a href="login.php?ac=logout">Logout</a></li>
					<li><a href="uploadPicture.php">Upload Picture</a></li>
					<li><a href="displayUser.php">User Page</a></li>
			<?php } 
				else { ?>
					<li><a href="login.php">Login</a></li>
					<li><a href="register.php">Registration</a></li>
			<?php } ?> 
			<li><a href="gallery.php">Gallery</a></li>					
        </ul>
        <div class="col-sm-4 col-md-4 pull-right">
            <div class="navbar-form" role="search">
                <div class="input-group">
                    <input id="search_input" type="text" class="form-control" placeholder="Search" name="q">
                    <div class="input-group-btn">
                        <button id="search_btn" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        </nav>     
    </div>
</nav>