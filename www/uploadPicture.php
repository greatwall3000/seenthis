<!DOCTYPE html>
<html>
<head>
    <title>Upload Picture</title>
    <?php include_once("navigation.php"); ?>
</head>
<body>
<div id="main" class="container">
<?php
    if(isset($_SESSION['user_info'])){
?>
<center>
    <div class="h1">Upload Image or Return to <a href="index.php">Home</a> Page</div>
    <form id="uploadPictureForm" enctype="multipart/form-data" action="doUpload.php" method="post">
        <font style="letter-spacing:1px" color="#FF0000">
        *Only allows uploading jpg|png|bmp|jpeg|gif images!!!<br/>
        *Max size is 1 MB</font><br/> 

        <input class="form-control required" type = "file" name = "picfile"/>
        <input type="hidden" name="MAX_FILE_SIZE" value="1000000">
    
        <label for="picname">Image Name: </label>
        <input id="picname" class="form-control required" type = "text" name = "picname" placeholder="Name this image" />

        <label for="tag">Image Tags: </label>
        <input id="tag" class="form-control required" type = "text" name = "tag" placeholder="Divide tags with space"/>
    
        <label class="err"> 
        <?php 
            if(isset($_GET["error"])){
                echo $_GET["error"];
            }
        ?>
        </label>
    
        <input type = "submit" value= "Submit"/>
        <input type = "reset" value = "Reset"/>
            
    </form> 
 </center>
 <?php } else {
        echo '<div class="h1">Please <a href="login.php">Login</a> to view this page</div>';
    } ?>
</div>
</body>
</html>