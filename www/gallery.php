<!DOCTYPE html>
<html>
<head>
        <title>My Gallery</title>
        <?php include_once("navigation.php"); ?>
    </head>
<body>
	<div id="main" class="container">
<?php
	if(isset($_GET["g"]) )
	{ 
		$g_userid = $_GET["g"]; 
	}
	else if( isset($_SESSION["user_info"]) and !isset($_GET["g"]) )
	{ 
		$g_username = $_SESSION["user_info"]["username"];
		$g_userid = $_SESSION["user_info"]["user_id"];

	}
	if( isset($g_userid) ) 
	{
		include("./dao/UsersDao.php");
		$g_username = (new UsersDao)->getUsernameById($g_userid);
	}

	if( isset($g_username) ) { 
		$gallery_header = "$g_username Gallery";
	}else{
		$gallery_header = "Please <a href='./login.php'>Login</a> to view your gallery";
	}

	include_once("./dao/PicturesDao.php");
	if (isset($g_userid)){
		$pictures = (new PicturesDao)->getUserGalleryById($g_userid);
	} else {
		$pictures = [];
	}
?>	
	<div class="row text-center">
		<div class="col-xs-12 gallery">
			<div class='container-fluid'><h2><?php echo $gallery_header ?></h2>		
			<?php 
			foreach ($pictures as $picture){ ?>				
				<a href='displayPicture.php?id=<?php echo $picture->id ?>' >
					<img class='corner-crop imgzoom img-responsive img-thumbnail' 
						 src='img/uploaded/<?php echo $picture->link ?>' 
						 alt='<?php $picture->picture_name ?>'/></a>				
			<?php 
			} echo "</div>";
			?>
		</div>
	</div>
</body>
</html>