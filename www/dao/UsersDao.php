<?php
	class UsersDao {		
		function login($email, $password){
			require_once("./config.php");
			$error = "No Connection to Database";
			$connection = getDBC();
			if($connection){
			
				$email = $connection->real_escape_string($email);
				if(!empty($email) && !empty($password) ){
					$sql = "SELECT `id`,`username` FROM `users` WHERE (`email` = ? AND `password` = ?) OR (`username` = ? AND `password` = ?)";

					$statement = $connection->prepare($sql);
					$statement->bind_param("ssss",$email,$password,$email,$password);
					$statement->execute();
					$statement->bind_result($g_userid,$g_username);
					$statement->fetch();

					if ( !empty($g_userid) && !empty($g_username) ){
						$_SESSION['user_info']['user_id'] = $g_userid;
						$_SESSION['user_info']['username'] = $g_username;
						$sql = "UPDATE `users` SET `last_login` = DATE(NOW()) WHERE `id`= $g_userid";
						$statement->close();
						$statement = $connection->prepare($sql);

						if($statement->execute()){
							$error = "Welcome, $g_username";
						}

					}else{
						$error = 'Wrong email or password.';
					}
					
					$statement->close();
				}else{
					$error = "Empty Field";
				}

				$connection->close();
			}
			return $error;
		}

		function register($username,$email,$password,$validation) {
			require_once("./config.php");

			$error = "No Connection to Database";
			$connection = getDBC();
			if($connection){
				
				if( $password)   $password   = sha1($password);
				if( $validation) $validation = sha1($validation);

				if ( !empty($username) && !empty($email) 
					&& !empty($password) && !empty($validation) ) 
				{
					if($password === $validation){

						$sql = "SELECT `username` FROM `users` WHERE `username`= ? OR `email`= ?";
						$statement = $connection->prepare($sql);
						$statement->bind_param("ss",$username,$email);
						$statement->execute();
						$statement->bind_result($g_username);
						$statement->fetch();
						$statement->close();

						if (isset($g_username) ){
							$error = "Username or Email Already Exists";
						}else{
							$sql = "INSERT INTO users(username, email, password, created) 
									VALUES(?, ?, ?, DATE(NOW()) )";

							$statement = $connection->prepare($sql);
							$statement->bind_param("sss",$username,$email,$password);
							if( $statement->execute() ){
								
								$error = "Thank You and Welcome";
							}
							$statement->close();
						}

					}else{
						$error = "Passwords Do Not Match";
					}
				}else{
					$error = "Something left blank";
				}

				$connection->close();
			}
			return $error;
		}

		function getUsernameById($u_id){
			require_once("./config.php");
			$g_username = "";
			
			$connection = getDBC();
			if($connection){
				
				$sql = "SELECT `username` FROM `users` WHERE `id`= ?";
				$statement = $connection->prepare($sql);
				$statement->bind_param("i",$u_id);
				$statement->execute();
				$statement->bind_result($g_username);
				$statement->fetch();
				$statement->close();

				$connection->close();
			}
			return $g_username;
		}

		function getUserByUserId($userID)
		{
			require_once("./config.php");
			require_once("./model/User.php");
			
			$currentUser = null;

			$connection = getDBC();
			if($connection)
			{
				$sql = "SELECT `username`, `email`, `profile_picture` FROM `users` WHERE `id`= ?";
				$statement = $connection->prepare($sql);
				$statement->bind_param("s",$userID);
				$statement->execute();
				$statement->bind_result($g_username, $g_email, $g_profile_picture);
				$statement->fetch();
				$statement->close();

				if(!empty($g_username))
				{
					if(!isset($g_profile_picture) || empty($g_profile_picture) 
						|| !file_exists("./img/avatar/".$g_profile_picture))
						$g_profile_picture = 'userDefinePicture.jpg';
					
					$currentUser = new User($userID, $g_username, $g_email, $g_profile_picture);
				}

				$connection->close();
			}

			return $currentUser;
		}

		function getUserByUsername($username)
		{
			if(! @include_once("./config.php")){
				@include_once("../config.php");
			}

			if(! @include_once("./model/User.php")){
				@include_once("../model/User.php");
			}

			$user = null;

			$connection = getDBC();
			if($connection)
			{
				$sql = "SELECT `id`, `email`, `profile_picture` FROM `users` WHERE `username`= ?";
				$statement = $connection->prepare($sql);
				$statement->bind_param("s",$username);
				$statement->execute();
				$statement->bind_result($g_id, $g_email, $g_profile_picture);
				$statement->fetch();
				$statement->close();

				if(!empty($g_id))
				{
					if(!isset($g_profile_picture) || empty($g_profile_picture) ){
						if(!file_exists("../img/avatar/".$g_profile_picture) || 
							!file_exists("./img/avatar/".$g_profile_picture) ){
							$g_profile_picture = 'userDefinePicture.jpg';
						}
					}
					$user = new User($g_id, $username, $g_email, $g_profile_picture);
				}

				$connection->close();
			}
			return $user;
		}
	}
?>