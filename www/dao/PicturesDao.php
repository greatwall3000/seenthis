<?php
class PicturesDao {

 	function getUserGalleryById($u_id){
  	  require_once("./config.php");
      require_once("./model/Picture.php");

  		$pictures = [];
  		$connection = getDBC();
  		if($connection){
    
  			$sql = "SELECT id, picture_name, link, user_id FROM `pictures` WHERE `user_id` = ? ORDER BY id DESC";
   			$statement = $connection->prepare($sql);
   			$statement->bind_param("i",$u_id);
   			$statement->execute();
   			$statement->bind_result($pic_id, $pic_name, $pic_link, $pic_user_id);
          
        while($statement->fetch()) {
          $picture = new Picture($pic_id, $pic_name, $pic_link, $pic_user_id);
          $pictures[] = $picture;
        }
 		
    		$statement->close();
    		$connection->close();
  		}
     	return $pictures;
 	}

 	function getAllPictures() {
  	  require_once("../config.php");
      require_once("../model/Picture.php");

  		$connection = getDBC();
  		if($connection){
    
   			$pictures = [];
   			$sql = "SELECT id, picture_name, link, user_id FROM `pictures` ORDER BY id DESC";
   			$statement = $connection->prepare($sql);
  			$statement->execute();
   			$statement->bind_result($pic_id, $pic_name, $pic_link, $pic_user_id);
   
   		  while($statement->fetch()) {
          $picture = new Picture($pic_id, $pic_name, $pic_link, $pic_user_id);
          $pictures[] = $picture;
        }
    		$statement->close();
    		$connection->close();
  		}
     	return $pictures;
 	}

  function getPicturesByMultiTag($search_string) {
      require_once("../config.php");
      require_once("../model/Picture.php");

      $pictures = [];
      $connection = getDBC();
      if($connection){
        $tags = array_unique( explode(" ",$search_string) );
    
        foreach($tags as $tag){
          $sql = "SELECT p.id, p.picture_name, p.link, p.user_id
                  FROM pictures p 
                  INNER JOIN pictures_tags pt 
                  ON p.id = pt.picture_id 
                  WHERE pt.tag_id = 
                  (SELECT id from tags WHERE tag_name = ?) 
                  ORDER BY id DESC";

          $statement = $connection->prepare($sql);
          $statement->bind_param("s",$tag);
          $statement->execute();
          $statement->bind_result($pic_id, $pic_name, $pic_link, $pic_user_id);
          
          while($statement->fetch()) {
            $picture = new Picture($pic_id, $pic_name, $pic_link, $pic_user_id);
            $pictures[] = $picture;
          }
          $statement->close();
        }
        $connection->close();
      }
      return $pictures;
  }

  function getPictureById($pic_id) {
      require_once("./config.php");
      require_once("./model/Picture.php");

      $picture = null;
      $connection = getDBC();
      if($connection){        
        $sql = "SELECT p.id, p.picture_name, p.link, p.user_id
                  FROM pictures p 
                  WHERE p.id = ?";

        $statement = $connection->prepare($sql);
        $statement->bind_param("s",$pic_id);
        $statement->execute();
        $statement->bind_result($pic_id, $pic_name, $pic_link, $pic_user_id);
        
        while($statement->fetch()) {
          $picture = new Picture($pic_id, $pic_name, $pic_link, $pic_user_id);  
        }
        $statement->close();
      }
      $connection->close();
    
    return $picture;
  }

  function addPicture($picname,$filename,$user_id,$width,$height,$size){
    require_once("./config.php");
    $connection = getDBC();
      if($connection){
        $sql = "INSERT INTO `pictures` (picture_name, link, created, user_id, width, height, size) VALUES (?,?,DATE(NOW()),?,?,?,?)";
        
        $statement = $connection->prepare($sql);
        $statement->bind_param("ssiiii", $picname, $filename, $user_id, $width, $height, $size);
        $statement->execute();
        $statement->close();
        $connection->close();
      }
    
  }

  function getPictureIdByLink($link){
  	require_once("./config.php");
    $connection = getDBC();
      if($connection){

        $sql = "SELECT id FROM pictures  
                  WHERE link = ?";

        $statement = $connection->prepare($sql);
        $statement->bind_param("s",$link);
        $statement->execute();
        $statement->bind_result($pic_id);
        
        $statement->fetch();
        if(isset($pic_id)){
          return $pic_id;
        }else{
          return -1;
        }

        $statement->close();
        $connection->close();
      }
  }

  function getUserIdByPictureId($id){
  	require_once("./config.php");
    $connection = getDBC();
      if($connection){

        $sql = "SELECT user_id FROM pictures  
                  WHERE id = ?";

        $statement = $connection->prepare($sql);
        $statement->bind_param("s",$id);
        $statement->execute();
        $statement->bind_result($user_id);
        
        $statement->fetch();
        if(isset($user_id)){
          return $user_id;
        }else{
          return -1;
        }

        $statement->close();
        $connection->close();
      }
  }

  function removeById($id){
  	require_once("./config.php");
  	$result = false;
  	$connection = getDBC();
      if($connection){

        $sql = "DELETE FROM pictures WHERE id = ?";

        $statement = $connection->prepare($sql);
        $statement->bind_param("s",$id);
        $statement->execute();

        if( $statement->affected_rows > 0){
			    $result = true;
		    }

        $statement->close();
        $connection->close();
      }
    return $result;
  }
}

?>