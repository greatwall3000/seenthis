<?php
class TagsDao {

 	function getTagIdByName($tag_name){

 		require_once("./config.php");
 		$connection = getDBC();
 		if($connection){
 			$sql = "SELECT id FROM `tags` WHERE `tag_name` = ?";
   			$statement = $connection->prepare($sql);
   			$statement->bind_param("s",$tag_name);
   			$statement->execute();
   			$statement->bind_result($tag_id);

   			$statement->fetch();
   			if(isset($tag_id)){
   				return (int)$tag_id;
   			}else{
   				return -1;
   			}
         $statement->close();
        	$connection->close();  
 		}


 	}

 	function addNewTag($tag_name){
 		require_once("./config.php");
 		$connection = getDBC();
 		if($connection){
 			if($this->getTagIdByName($tag_name)<=0){
 				$sql = "INSERT INTO tags (`tag_name`) VALUES(?)";
   			$statement = $connection->prepare($sql);
   			$statement->bind_param("s",$tag_name);
   			$statement->execute();
            $statement->close();
 			}
 			$connection->close();
 		}
 	}

	function addPicturesTags($pic_id,$tag_id){
		require_once("./config.php");
 		$connection = getDBC();
 		if($connection){
 			$sql = "INSERT INTO `pictures_tags` (`picture_id`,`tag_id`) VALUES(?,?)";
			$statement = $connection->prepare($sql);
			$statement->bind_param("ii",$pic_id,$tag_id);
			$statement->execute();

         $statement->close();
 		   $connection->close();
 		}
	}

   function removeTagById($tag_id){
      require_once("./config.php");
      $connection = getDBC();
      if($connection){

         $sql = "SELECT * FROM `pictures_tags` WHERE `tag_id` = ?";
         $statement = $connection->prepare($sql);

         $statement->bind_param("i",$tag_id);
         $statement->execute();

         $statement->store_result();
         $rows = $statement->num_rows;
         $statement->close();

         if( $rows == 0){

            $sql = "DELETE FROM tags WHERE id = ?";
            $statement = $connection->prepare($sql);
            $statement->bind_param("i",$tag_id);
            $statement->execute();
            $statement->close();
            
         }

         $connection->close();
         
      }
   }

   function getPictureTagsById($pic_id){
      require_once("./config.php");
      $connection = getDBC();
      $tags = array();
      if($connection){

         $sql = "SELECT `tag_id` FROM `pictures_tags` WHERE `picture_id` = ?";
         $statement = $connection->prepare($sql);

         $statement->bind_param("i",$pic_id);
         $statement->execute();

         $statement->bind_result($tag_id);
         $statement->store_result();
         $rows = $statement->num_rows;

         if($rows > 0){
            while($statement->fetch()) {
               array_push($tags, $tag_id);
            }
         }

         $statement->close();
         $connection->close();
      }
      return $tags;
   }
}
?>