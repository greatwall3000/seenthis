<?php

class CommentsDao {

  function getCommentsByPictureId($pic_id) {
      require_once("./config.php");
      require_once("./model/Comment.php");

      $comments = [];

      $connection = getDBC();
      if($connection){
        $sql = "SELECT id, user_id, picture_id, comment_text, created
                FROM comments
                WHERE picture_id = ?";

        $statement = $connection->prepare($sql);
        $statement->bind_param("s",$pic_id);
        $statement->execute();
        $statement->bind_result($c_id, $c_user_id, $c_picture_id, $c_comment_text, $c_created);
        
        while($statement->fetch()) {
          $comment = new Comment($c_id, $c_user_id, $c_picture_id, $c_comment_text, $c_created);
          $comments[] = $comment;
        }
        $statement->close();
          
      }
      $connection->close();
      
      return $comments;
  }

  function addCommentToPicture($pic_id, $user_id, $comment_text){
    require_once("./config.php");
    $connection = getDBC();
    if($connection){


      $sql = "INSERT INTO `comments` (`user_id`,`picture_id`,`comment_text`,`created`) VALUES (?,?,?,DATE(NOW()))";
      
      $statement = $connection->prepare($sql);
      $statement->bind_param("iis", $user_id, $pic_id, $comment_text);
      $statement->execute();
      $statement->close();
    }
    $connection->close();
  }
}
?>