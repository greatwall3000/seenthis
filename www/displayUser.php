<!DOCTYPE html>
<?php 
    include_once("navigation.php");
    include_once("./dao/UsersDao.php");

    if( isset($_GET["u"]) ){   
        $currentUser = (new UsersDao)->getUserByUsername(
            (new UsersDao)->getUsernameById($_GET["u"]));
        if($currentUser == null){
            header("Location: error.php");
        }
    }
    else if(!isset($_SESSION['user_info']) || !is_array($_SESSION['user_info']))
    {   
        header("Location: login.php");
    }
    else
    {
        $currentUser = (new UsersDao)->getUserByUsername($_SESSION['user_info']['username']);
    }

    include_once("./dao/PicturesDao.php");
    $pictures = (new PicturesDao)->getUserGalleryById($currentUser->id);
?>
<html>
<head>
    <title>Display User Page</title>
    <script type="text/javascript">
        $(document).ready(function() 
        {
            $(".btn-pref .btn").click(function () 
            {
                $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
                $(this).removeClass("btn-default").addClass("btn-primary");   
            });
        });
    </script>
</head>
<body>
    <div id="main" class="container">
	<div class="col-md-2 col-s m-2"></div>
	<div class="col-md-8 col-s m-8">
        <div class="card hovercard">
            <div class="card-background">
                <img class="card-bkimg" alt=""  align="middle" src="img/bg.jpg">          
            </div>
            <div class="useravatar">
               <img src="img/avatar/<?php echo $currentUser->profile_picture ?>"/>
            </div>
            <div class="card-info"> 
                <span class="card-title"><?php echo $currentUser->username ?></span><br/>
                <span class="card-title"><?php echo $currentUser->email ?></span>
            </div>
        </div>
        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <button type="button" id="Setting" class="btn btn-primary" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                    <div class="hidden-xs">Setting Up</div>
                </button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" id="gallery" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    <div class="hidden-xs">Gallery</div>
                </button>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tab-content">           
                 <div class="tab-pane fade in active" id="tab1">
                    <h3>This is tab 1</h3>
                </div>
                <div class="tab-pane fade in" id="tab3">
                    <div class="row text-center">
                        <div class="col-xs-12 gallery">
                            <?php 
                            foreach ($pictures as $picture){ ?>                           
                                <a href='displayPicture.php?id=<?php echo $picture->id ?>' >
                                    <img class='corner-crop imgzoom img-responsive img-thumbnail' 
                                         src='img/uploaded/<?php echo $picture->link ?>' 
                                         alt='<?php $picture->picture_name ?>'/></a>
                                
                            <?php } echo "</div>"; ?>
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
    </div>
</body>
</html>