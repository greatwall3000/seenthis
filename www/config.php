<?php
function getDBC() {
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	/* Define MySQL connection details and database table name */ 
	$SETTINGS["hostname"] = 'localhost';
	$SETTINGS["mysql_user"] = 'root';
	$SETTINGS["mysql_pass"] = '';
	$SETTINGS["mysql_database"] = 'seenthis_db';

	try{ 
		$connection = new mysqli(
			$SETTINGS["hostname"], 
			$SETTINGS["mysql_user"], 
			$SETTINGS["mysql_pass"], 
			$SETTINGS["mysql_database"] );
		return $connection;
	}
	catch (mysqli_sql_exception $e) {
		return null;
	}
}
?>