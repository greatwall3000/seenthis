<!DOCTYPE html>
<html>
    <head>
        <title>SeenThis Home Page</title>
    	<?php include_once("navigation.php"); ?>        
    </head>
	<body>
		<div id="main" class="container">
			<div class="row text-center">
				<div class="h1">SEENTHIS GALLERY</div>
			    <div id="search_result_div" class="col-xs-12 gallery">
			    </div>
			</div>
		</div>
	</body>
</html>