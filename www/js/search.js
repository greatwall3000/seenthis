$(document).ready(function()
{
    function appendSearchResult(id, link, name, isSearchByUser)
    {
        if(isSearchByUser){
            $("#search_result_div").append('<a href="displayUser.php?u='+id+'" ><img class="round-clip imgzoom img-responsive img-thumbnail" border="0" src="img/avatar/'+link+
                        '" height=100 width=100/></a>');
        }
        else{
               $("#search_result_div").append('<a href="displayPicture.php?id='+id+'" ><img class="corner-crop imgzoom img-responsive img-thumbnail" border="0" src="img/uploaded/'+link+
                        '" height=100 width=100/></a>');
        }
    }

    $("#search_btn").click(function(e)
    {
        var search_content = $('#search_input').val();
        if(search_content.length != 0)
        {   
        	$.ajax
        	({
        		url: "handler/searchHandler.php",
        		type: "POST",
        		data: {search_content: search_content},
        		success: function (jsonResult) 
        		{
                    if(jsonResult.length != 2)
                    {
                        $("#main").html('<div class="container"><div class="row text-center"><div id="search_result_div" class="col-xs-12 gallery"></div></div></div>');
                        var result = $.parseJSON(jsonResult);

                        if(!Array.isArray(result)){
                            var temp = result;
                            result = [];
                            result.push(temp);
                        }

                        for (var i=0;i<result.length;i++)
                        {                               
                            if(result[i].username != null)
                            { 
                                appendSearchResult(result[i].id, result[i].profile_picture, result[i].username, true);
                            }else{
                                appendSearchResult(result[i].id, result[i].link, result[i].picture_name, false);
                            }                            
                        }                        
                    }
                    else
                    {
                        alert("Sorry, we cannot find any result:(");
                    }
      			},
        		error: function (request, status, error) 
        		{
                    alert(request.responseText);
        			console.log(request.responseText);
    			}
        	});
        }
    });

    $( document ).ready(function(e)
    {
        var display_content = "all";
        $.ajax
        ({
            url: "handler/searchHandler.php",
            type: "POST",
            data: {display_content: display_content},
            success: function (jsonResult) 
            {
                if(jsonResult.length != 2)
                {
                    $("#search_result_div").html('');
                    var result = $.parseJSON(jsonResult);
                    if(!Array.isArray(result)){
                        var temp = result;
                        result = [];
                        result.push(temp);
                    }
                    for (var i=0;i<result.length;i++)
                    {   
                        if(result[i].username != null)
                        { 
                            appendSearchResult(result[i].id, result[i].profile_picture, result[i].username, true);
                        }else{
                            appendSearchResult(result[i].id, result[i].link, result[i].picture_name, false);
                        }
                    }                    
                }
                else
                {
                    alert("Sorry, we cannot find any result:(");
                }
            },
            error: function (request, status, error) 
            {
                alert(request.responseText);
                console.log(request.responseText);
            }
        });
        
    });
});