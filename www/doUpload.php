<?php
session_name('LoginForm');
@session_start();

if(!isset($_SESSION['user_info']) || !is_array($_SESSION['user_info']))
{
    echo "<script> window.location.assign('login.php'); </script>";
}

require_once('config.php');
$connection = getDBC();

$upfile = $_FILES["picfile"];
$typelist = ["image/jpeg","image/jpg","image/png","image/gif"];
$path = "./img/uploaded/";

if($upfile["error"]>0){
    switch($upfile['error']){
        case 1:
            $info="Image exceeds the value of upload_max_filesize in php.ini.";
            break;
        case 2:
            $info="Image exceeds the value of upload_max_filesize in HTML form.";
            break;
        case 3:
            $info="Only part of the image is uploaded.";
            break;
        case 4:
            $info="There is no image uploaded.";
            break;
        case 6:
            $info="Cannot find the temp file folder.";
            break;
        case 7:
            $info="File input failed.";
            break;
    }
    header("Location: uploadPicture.php?error=Upload Image False, Reason: ".$info);
}

if($upfile["size"]>1000000){
    header("Location: uploadPicture.php?error=The uploading file exceeds the limitation");
}

if(!in_array($upfile["type"],$typelist)){
    header("Location: uploadPicture.php?error=The type of file is invalid!".$upfile["type"]);
}

$fileinfo = pathinfo($upfile["name"]);
do{
    $newfile = date("YmdHis").rand(1000,9999).".".$fileinfo["extension"];
}while(file_exists($newfile));

if(is_uploaded_file($upfile["tmp_name"])){
    $size = $upfile['size'];
    $image_info = getimagesize($_FILES["picfile"]["tmp_name"]);
    $width = $image_info[0];
    $height = $image_info[1];

    if(move_uploaded_file($upfile["tmp_name"], $path.$newfile)){
        
        $picname = $_POST["picname"];
        $user_id = $_SESSION['user_info']['user_id'];
        $filename = $newfile;      
        $addtime = date('Y-m-d H:i:s');
        
        $tagname = $_POST["tag"];
        
        if(empty($tagname)){
            header("Location: uploadPicture.php?error=You have to type Tag name!");
        }

        include_once("./dao/TagsDao.php");
        $tagsDao = new TagsDao();
        $tags = explode(' ', $_POST['tag']);
        foreach ($tags as $tagname) 
        {
          $tagsDao->addNewTag($tagname);
        }

        include_once("./dao/PicturesDao.php");
        $picDao = new PicturesDao();
        $picDao->addPicture($picname,$filename,$user_id,$width,$height,$size);

        foreach ($tags as $tagname) 
        { 
          $tag_id = $tagsDao->getTagIdByName($tagname);
          echo "in loop 1 <br/>";
          $picture_id = $picDao->getPictureIdByLink($filename);
          echo "in loop 2 <br/>".$tag_id." & ".$picture_id;
          if($tag_id>0 && $picture_id>0) {
            $tagsDao->addPicturesTags($picture_id, $tag_id);
          }
        }

        header("Location: uploadPicture.php?error=Picture Added to Gallery");        
    }else{
        header("Location: uploadPicture.php?error=Upload Image Failed!");
    }
}else{    
    header("Location: uploadPicture.php?error=This is not a valid upload file");
}
?>