<?php
	$_SESSION['message'] ="";
	if (isset($_POST['submit'])) 
	{
		$username = $_POST['username'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$validation = $_POST['validation'];

		include_once("./dao/UsersDao.php");
		$_SESSION["message"] = (new UsersDao)->register($username,$email,$password,$validation);
		if($_SESSION["message"] === "Thank You and Welcome"){
			echo "<script>alert('You have been registred');window.location = 'login.php'</script>";
		}
	} 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Registration</title>
     	<?php include_once("navigation.php"); ?>
    </head>
	<body>
<body>
<form id="registration-form" class="registration-form" name="form2" method="post" action="register.php">
	    	<input type="hidden" name="is_register" value="1">
	        <div class="h1">Register</div>
	        <div id="form-content">
	            <div class="group">
	             <label for="username">Username</label>
	                <div><input id="username" name="username" class="form-control required" type="text" placeholder="enter your username"></div>   
	            </div>
	            <div class="group">
		           	<label for="email">Email</label>
		            <div><input id="email" name="email" class="form-control required" type="text" placeholder="enter your email"></div>
	            </div>
	            <div class="group">    
	             	<label for="password">Password</label>
	                <div><input id="password" name="password" class="form-control required" type="password" placeholder="enter your password"></div>
	            </div>
	            <div class="group">
	            	<label for="name">Confirm Password</label>
	                <div><input id="validation" name="validation" class="form-control required" type="password" placeholder="retype password"></div>
	            </div>						
	            <div class="group submit">
	              	<label class="err">
	              	<?php 
	              		if(isset($_SESSION["message"]))
	              			echo $_SESSION["message"];
	              	?>	              		
	              	</label>
	                <div><input name="submit" type="submit" value="Submit"/></div>
	            </div>
	        </div>
</body>
</html>