<?php
session_name('LoginForm');
@session_start();

if(isset($_SESSION["user_info"]) && is_array($_SESSION["user_info"])){
	$s_user_id = $_SESSION["user_info"]["user_id"];
}else{
	header("Location: error.php");
}

if(isset($_GET["pid"]) )
{ 
	$g_picture_id = $_GET["pid"];
}else{
	header("Location: error.php");
}

require_once("./dao/PicturesDao.php");
require_once("./dao/TagsDao.php");
$t_dao = new TagsDao();
$p_dao = new PicturesDao();
$picture_owner = $p_dao->getUserIdByPictureId($g_picture_id);
$filepath = "./img/uploaded/".($p_dao->getPictureById($g_picture_id)->link);
$picture_tags = $t_dao->getPictureTagsById($g_picture_id);

if($picture_owner == $s_user_id){
	if($p_dao->removeById($g_picture_id)){
		if (is_file($filepath))
		{
			unlink($filepath);
		}
		
		foreach($picture_tags as $tag){
			$t_dao->removeTagById($tag);
		}

		echo "<script> alert('Picture Deleted'); window.location = 'index.php';</script>";
	}else{
		echo "<script> alert('Picture Was Not Deleted'); 
				window.location = 'index.php';</script>";
	}	
}else{
	header("Location: error.php");
}
?>