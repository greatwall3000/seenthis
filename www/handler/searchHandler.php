<?php 	
	if(isset( $_POST['search_content'] ))
	{	
		
		$search_content = $_POST['search_content'];
		
		//$searchDao = new SearchDao();
		//$result = $searchDao->searchByContent($search_content);
		require_once("../dao/PicturesDao.php");
		$result = (new PicturesDao)->getPicturesByMultiTag($search_content);
		
		
		if(empty($result)){
			$result = getUserDetails($search_content);
		}else{
			$users_found = array_filter( getUserDetails($search_content) );
			if( is_array($users_found)  ){
				$result = array_merge($users_found, $result);
			}else{
				array_push( $result, $users_found );
			}
			
		}
		// if we search username it will only has one result, however if we search tag name, it will return a array.
		if(is_array($result))
			echo(json_encode($result));
		else
			echo json_encode((array)$result);
		
		
	}
	else if(isset( $_POST['display_content'] ))
	{
		require_once("../dao/PicturesDao.php");
		$result = (new PicturesDao)->getAllPictures();

		if(is_array($result))
			echo(json_encode($result));
		else
			echo json_encode((array)$result);
	}

	function getUserDetails($search_st){
		require_once("../dao/UsersDao.php");
		$result_users = [];
		$search_ar = array_unique( explode(" ",$search_st) );
		foreach($search_ar as $st){
			$user_data = (new UsersDao)->getUserByUsername($st);
			if(!empty($user_data)){
				array_push($result_users, $user_data );
			}
			
		}
		return $result_users;
	}	
?>