<!DOCTYPE html>
    <head>
        <title>Login</title>
        <?php include_once("navigation.php"); ?>
    </head>
	<body>
<?php
	if(isset($_POST['is_login'])){
		include("./dao/UsersDao.php");

		$email = $_POST['email'];
		$password = $_POST['password'];
		if($password) $password = sha1($password);
		
		$_SESSION["message"] = (new UsersDao)->login($email, $password);
	}

	$error = "";
	if( isset($_SESSION["message"]) ){ $error = $_SESSION["message"]; }

	if(isset($_GET['ac']) && $_GET['ac'] == 'logout'){
		$_SESSION['user_info'] = null;
		unset($_SESSION['user_info']);
		$_SESSION['message'] = null;
		unset($_SESSION['message']);
		$error = "";
		header("Location: index.php");
	}
?>
	<?php if(isset($_SESSION['user_info']) && is_array($_SESSION['user_info'])) { 
		header("Location: displayUser.php");
	 } else { ?>
	 	<div id="main" class="container">
	    <form id="login-form" class="login-form" name="form1" method="post" action="login.php">
	    	<input type="hidden" name="is_login" value="1">
	        <div class="h1">login</div>
	        <div id="form-content">
	            <div class="group">
	                <label for="email">Email / Username</label>
	                <div><input id="email" name="email" class="form-control required" type="text" placeholder="enter email or username"></div>
	            </div>
	           <div class="group">
	                <label for="name">Password</label>
	                <div><input id="password" name="password" class="form-control required" type="password" placeholder="enter password"></div>
	            </div>
	            <?php if($error) { ?>
					<label class="err" for="password" generated="true" style="display: block;"><?php echo $error ?></label>
				<?php } ?>
	            <div class="group submit">
	                <label class="empty"></label>
	                <div><input name="submit" type="submit" value="Submit"/></div>
	            </div>
	        </div>
	        <div id="form-loading" class="hide"><i class="fa fa-circle-o-notch fa-spin"></i></div>
	    </form>
	    </div>
	<?php } ?>   
    </body>
</html>