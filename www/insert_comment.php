<?php
session_name('LoginForm');
@session_start();

if(isset($_SESSION["user_info"]) && is_array($_SESSION["user_info"])){
	$s_user_id = (int)$_SESSION["user_info"]["user_id"];
	
	if(isset($_POST["cmt_picture_id"]) && isset($_POST["comment_text"]) ){
		$p_cmt_picture_id = (int)$_POST["cmt_picture_id"];
		$p_cmt_txt = $_POST["comment_text"];

		if( !empty($_POST["comment_text"]) ){
			include_once("./dao/CommentsDao.php");
			(new CommentsDao)->addCommentToPicture($p_cmt_picture_id, $s_user_id, $p_cmt_txt);
		}
		header("Location: ./displayPicture.php?id=".$p_cmt_picture_id);
	}
}
?>