SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `seenthis_db`
--
CREATE DATABASE IF NOT EXISTS `seenthis_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `seenthis_db`;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `picture_id` int(11) NOT NULL,
  `comment_text` varchar(512) DEFAULT NULL,
  `created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `picture_id`, `comment_text`, `created`) VALUES
(1, 15, 2, 'That is nice', '2016-10-24'),
(2, 15, 3, 'That is cool', '2016-10-24'),
(3, 13, 12, 'I am BATMAN', '2016-11-11'),
(4, 15, 12, 'NANA-NANA-NANA-NANA BATMAAAAN!', '2016-11-11'),
(5, 14, 13, 'These don''t grow where I live', '2016-11-11'),
(6, 13, 15, 'That is some happy dog', '2016-11-11'),
(7, 13, 14, 'This dog is like WOW', '2016-11-11');

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

DROP TABLE IF EXISTS `favourites`;
CREATE TABLE `favourites` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `picture_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

DROP TABLE IF EXISTS `pictures`;
CREATE TABLE `pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `picture_name` varchar(128) DEFAULT NULL,
  `height` int(7) NOT NULL,
  `width` int(7) NOT NULL,
  `size` int(11) NOT NULL,
  `link` varchar(128) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`id`, `picture_name`, `height`, `width`, `size`, `link`, `user_id`, `created`) VALUES
(1, 'Cat in paper', 1200, 1600, 139264, 'catroll.jpg', 13, '2016-10-18'),
(2, 'Cat with crown', 720, 856, 73728, 'catking.jpg', 15, '2016-10-18'),
(3, 'Black cat', 353, 632, 114688, 'blackcat.jpg', 15, '2016-10-18'),
(4, 'Colored cats', 194, 259, 12288, 'rainbowcats.jpg', 15, '2016-10-18'),
(5, 'Star Wars Cat Vehicle', 168, 300, 8192, 'swcat.jpg', 15, '2016-10-18'),
(6, 'Cats with gun', 168, 300, 12288, 'snipercats.jpg', 15, '2016-10-18'),
(7, 'Cat in a hat', 428, 429, 413696, 'cathat.png', 13, '2016-10-18'),
(8, 'Cat with a bread on it''s face', 160, 284, 8192, 'breadcat.jpg', 15, '2016-10-18'),
(9, 'Grumpy cat', 190, 266, 8192, 'grumpycat.jpg', 15, '2016-10-18'),
(10, 'Weird color cat', 255, 255, 8192, '2colorcat.jpg', 15, '2016-10-18'),
(11, 'Cat eyes', 400, 700, 106496, 'cateyes.jpg', 13, '2016-10-18'),
(12, 'batman dog', 525, 450, 61763, 'dog_batman.jpg', 13, '2016-11-11'),
(13, 'Dog that looks like flower', 768, 1024, 86016, 'dog_flower.jpg', 13, '2016-11-11'),
(14, 'Dog looks amazed', 360, 480, 20480, 'dog_wondering.jpg', 16, '2016-11-11'),
(15, 'Smiling dog', 1200, 1920, 376832, 'happy_dog.jpg', 16, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `pictures_tags`
--

DROP TABLE IF EXISTS `pictures_tags`;
CREATE TABLE `pictures_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `picture_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pictures_tags`
--

INSERT INTO `pictures_tags` (`id`, `picture_id`, `tag_id`) VALUES
(1, 3, 1),
(2, 1, 1),
(3, 5, 1),
(4, 9, 1),
(5, 2, 1),
(6, 4, 1),
(7, 7, 1),
(8, 6, 1),
(9, 12, 2),
(10, 13, 2),
(11, 14, 2),
(12, 15, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `tag_name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag_name`) VALUES
(1, 'cat'),
(4, 'darknes'),
(2, 'dog'),
(3, 'nature'),
(5, 'sun');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `validation` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(64) DEFAULT NULL,
  `profile_picture` varchar(128) DEFAULT NULL,
  `email_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `last_login` date DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `validation`, `email`, `profile_picture`, `email_hidden`, `last_login`, `created`, `modified`) VALUES
(13, 'JohnLennon', '2ae868079d293e0a185c671c7bcdac51df36e385', 1, 'johnlennon@gmail.com', 'johnlennon_profilepic.jpg', 0, NULL, NULL, NULL),
(14, 'IvanSusanin', 'c893e8dee1c653d0e0c35099a74172ef8364b133', 0, 'susanin@boloto.ru', 'ivansusanin_profilepic.jpg', 0, NULL, NULL, NULL),
(15, 'PeterPan', '02fe017663a67cb358026c6dae89e8429cd69e18', 1, 'peterpan@gmail.com', 'peterpan_profilepic.jpg', 1, '2016-11-10', NULL, NULL),
(16, 'JohnSnow', '366a2fd21ce0484422c9ead06b82eb8a76286235', 1, 'snow@snow.com', 'johnsnow_profilepic.jpg', 0, NULL, NULL, NULL),
(17, 'icecreamman', '58c3a631a71ff44779dc6276a179c0950abcfe88', 1, 'ice@cream.man', 'iecreamman_profilepic.jpg', 0, NULL, NULL, NULL),
(18, 'cat', '9d989e8d27dc9e0ec3389fc855f142c3d40f0c50', 0, 'cat@cat.cat', 'cat_profilepic.jpg\r\n', 0, NULL, '2016-11-11', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD KEY `picture_id` (`picture_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `picture_id` (`picture_id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `pictures_tags`
--
ALTER TABLE `pictures_tags`
  ADD KEY `picture_id` (`picture_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD KEY `tag_name` (`tag_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `pictures_tags`
--
ALTER TABLE `pictures_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`picture_id`) REFERENCES `pictures` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favourites`
--
ALTER TABLE `favourites`
  ADD CONSTRAINT `favourites_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favourites_ibfk_2` FOREIGN KEY (`picture_id`) REFERENCES `pictures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pictures_tags`
--
ALTER TABLE `pictures_tags`
  ADD CONSTRAINT `pictures_tags_ibfk_1` FOREIGN KEY (`picture_id`) REFERENCES `pictures` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pictures_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;
